import pygame
class Kingdom:
    def __init__(self,screen,hp):
        self.screen=screen
        self.hp=hp
    
    def dealdamage(self,x):
        self.hp-=x
    
    def drawHp(self, screen):
        teksti_font = pygame.font.Font(None, 25)
        description=teksti_font.render(str(self.hp)+"HP",1,[0,0,0])
        screen.blit(description,[645,500])
        
    def getHP(self):
        return self.hp
