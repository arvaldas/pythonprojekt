import pygame
class Bullet:
    
    def __init__(self,x,y,enemy,speed,screen,gun,color,damage):
        self.enemy=enemy
        self.screen=screen
        self.x=x
        self.y=y
        self.gun=gun
        self.damage=damage
        self.color=color
        self.speed=speed
        self.alive=True
    
    
    def bullet_end(self):
        return (self.enemy.getX(),self.enemy.getY())
    
    def draw(self):
        pygame.draw.ellipse(self.screen,self.color,[self.x,self.y,2,2],0)
        
    def MoveToCordinateControl(self,xStart,yStart,xEnd,yEnd):
        if abs(xStart-xEnd)>self.speed or abs(yStart-yEnd)>self.speed:
            return self.MoveToCordinate(xStart,yStart,xEnd,yEnd)
        else:
            return xEnd,yEnd
    
    def finishMetod(self):
        self.draw()
        XY=self.MoveToCordinateControl(self.x,self.y,self.bullet_end()[0],self.bullet_end()[1])
        self.x=XY[0]
        self.y=XY[1]
        self.touch()
    
    
    def touch(self):
        if (self.x-self.bullet_end()[0])**2+(self.y-self.bullet_end()[1])**2<9:
            self.alive=False
            self.gun.setReady(True)
            self.enemy.dealdamage(self.damage)
            
            
    def getAlive(self):
        return self.alive
    
    def MoveToCordinate(self,xStart,yStart,xEnd,yEnd):#Метод для кординатов
        xMovement = self.speed * (xEnd - xStart)**2/((yEnd - yStart)**2+(xEnd - xStart)**2)
        yMovement = self.speed - xMovement
        if xStart<xEnd:
            xStart+=xMovement
        elif xStart>xEnd:
            xStart-=xMovement
            
        if yStart<yEnd:
            yStart+=yMovement
        elif yStart>yEnd:
            yStart-=yMovement
            
        return xStart,yStart
        
    def shoot(self,info):
        pass
        

class Gun:
    
    def __init__(self,radius,x,y,color):
        self.radius = radius
        self.x=x
        self.y=y
        self.color=color
        self.enemy = None
        self.Ready=True
        
    def getX(self):
        return self.x
    def getY(self):
        return self.y
    def getColor(self):
        return self.color
    def getEnemy(self):
        return self.enemy
    
    def setReady(self,x):
        self.Ready=x
    
    def shoot(self):
        pass
        
    def checkInRaius(self,info):
        for i in info:
            if self.radius**2 > (self.x-i.getX())**2 + (self.y-i.getY())**2:
                if self.Ready==True:
                    self.enemy = i
                    return True
                else:
                    return False
                #bullet = Bullet(i)
        
        
        
        