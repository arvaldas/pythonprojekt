import pygame
from random import randint

class Fat_Man:
    def __init__(self,screen,x,y,speed,hp,kingdome,money,checkpoint,level):
        self.screen=screen
        self.x=x
        self.kingdome=kingdome
        self.y=y
        self.hp=hp
        self.level=level
        self.speed=speed
        self.checkpoint = checkpoint
        self.muutuja = 0
        self.finish=False
        self.startHP=hp
        self.salvastaja = (420,randint(35,105))
        self.money=money
        
    def getX(self):
        return self.x
    def getY(self):
        return self.y
    def getHp(self):
        return self.hp
    def dealdamage(self,x):
        self.hp-=x
    def getFinish(self):
        return self.finish
    def getMoney(self):
        return self.money
    def getCheckpoint(self):
        return self.checkpoint
    def getLevel(self):
        return self.level
    def getstartHP(self):
        return self.startHP
    
        
        
    def turn1(self):
        return (420,randint(35,135))
        
    def turn2(self):
        return (randint(385,420),250)
        
    def turn3(self):
        return (70,randint(210,245))
        
    def turn4(self):
        return (randint(70,105),430)
        
    def turn5(self):
        return (345,randint(410,435))
        
    def turn6(self):
        return (randint(315,385),350)

    def MoveToCordinateControl(self,xStart,yStart,xEnd,yEnd):
        if abs(xStart-xEnd)>self.speed or abs(yStart-yEnd)>self.speed:
            return self.MoveToCordinate(xStart,yStart,xEnd,yEnd)
        else:
            return xEnd,yEnd

    def MoveToCordinate(self,xStart,yStart,xEnd,yEnd):#Метод для кординатов
        if xStart<xEnd:
            xStart+=self.speed
        elif xStart>xEnd:
            xStart-=self.speed
            
        if yStart<yEnd:
            yStart+=self.speed
        elif yStart>yEnd:
            yStart-=self.speed
            
        return xStart,yStart
    
    def draw(self,img):
        if self.hp > 0:
            pygame.draw.rect(self.screen,[0,255,0],[self.x-4.5,self.y-7.5,24,5],1)
            pygame.draw.rect(self.screen,[0,255,0],[self.x-4.5,self.y-7.5,24*self.hp/self.startHP,4],0)
            self.screen.blit(img,(self.x,self.y))
        if self.hp<self.startHP:
            self.hp+=2
                        
            
        
    def checkPoint1(self,x,y):
        self.x=x
        self.y=y
        print(x)
        
    def finishMethod(self):
        if self.x>520:
            self.hp=0
            self.finish=True
           
        if self.checkpoint==1:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn1()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.x>=420:
                self.checkpoint = 2
                self.salvastaja = (400,250)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 2:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn2()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.y>=250:
                self.checkpoint = 3
                self.salvastaja = (70,225)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 3:
            if self.muutuja % 15 == 1:
                self.salvastaja = self.turn3()
            xy = self.salvastaja
            
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.x<=70:
                self.checkpoint = 4
                self.salvastaja = (85,455)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 4:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn4()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.y>=429:
                self.checkpoint = 5
                self.salvastaja = (345,420)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 5:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn5()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.x>=345:
                self.checkpoint = 6
                self.salvastaja = (350,350)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 6:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn6()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.y<=350:
                self.checkpoint = 7
            self.muutuja+=1
        
        
        elif self.checkpoint == 7:
            self.x +=self.speed

class Enemy:
    def __init__(self,screen,x,y,speed,hp,kingdome,money):
        self.screen=screen
        self.x=x
        self.kingdome=kingdome
        self.y=y
        self.hp=hp
        self.speed=speed
        self.checkpoint = 1
        self.muutuja = 0
        self.finish=False
        self.startHP=hp
        self.salvastaja = (420,randint(35,105))
        self.money=money
        
    def getX(self):
        return self.x
    def getY(self):
        return self.y
    def getHp(self):
        return self.hp
    def dealdamage(self,x):
        self.hp-=x
    def getFinish(self):
        return self.finish
    def getMoney(self):
        return self.money
    def getCheckpoint(self):
        return self.checkpoint
    
        
    #Igal pöörel genereeritakse randomselt uus asukoht, kuhu peab vaenlane liikuma
    def turn1(self):
        return (420,randint(35,118))
        
    def turn2(self):
        return (randint(385,420),250)
        
    def turn3(self):
        return (70,randint(210,245))
        
    def turn4(self):
        return (randint(70,105),430)
        
    def turn5(self):
        return (345,randint(410,435))
        
    def turn6(self):
        return (randint(315,385),350)

    #Järgenvad kaks meetodid genereerivad vaenlase kaootilist teekonda
    def MoveToCordinateControl(self,xStart,yStart,xEnd,yEnd):
        if abs(xStart-xEnd)>self.speed or abs(yStart-yEnd)>self.speed:
            return self.MoveToCordinate(xStart,yStart,xEnd,yEnd)
        else:
            return xEnd,yEnd

    def MoveToCordinate(self,xStart,yStart,xEnd,yEnd):#Koordinaatide meetod
        if xStart<xEnd:
            xStart+=self.speed
        elif xStart>xEnd:
            xStart-=self.speed
            
        if yStart<yEnd:
            yStart+=self.speed
        elif yStart>yEnd:
            yStart-=self.speed
            
        return xStart,yStart
    
    def draw(self,img):
        if self.hp > 0:
            pygame.draw.rect(self.screen,[0,255,0],[self.x-1.5,self.y-7.5,24,5],1)
            pygame.draw.rect(self.screen,[0,255,0],[self.x-1.5,self.y-7.5,24*self.hp/self.startHP,4],0)
            self.screen.blit(img,(self.x,self.y))
    
    #checkPoint meetodid vastutavad vaenlase pöörete eest
    def checkPoint1(self,x,y):
        self.x=x
        self.y=y
        print(x)
    
    #Vaenlase peameetod, kus kutsutakse välja kõik teised meetodid peale draw()
    def finishMethod(self):
        if self.x>520:
            self.hp=0
            self.finish=True
            
            
        if self.checkpoint==1:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn1()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.x>=420:
                self.checkpoint = 2
                self.salvastaja = (400,250)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 2:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn2()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.y>=250:
                self.checkpoint = 3
                self.salvastaja = (70,225)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 3:
            if self.muutuja % 15 == 1:
                self.salvastaja = self.turn3()
            xy = self.salvastaja
            
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.x<=70:
                self.checkpoint = 4
                self.salvastaja = (85,455)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 4:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn4()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.y>=429:
                self.checkpoint = 5
                self.salvastaja = (345,420)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 5:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn5()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.x>=345:
                self.checkpoint = 6
                self.salvastaja = (350,350)
            self.muutuja+=1
            
            
            
        elif self.checkpoint == 6:
            if self.muutuja % 15 == 0:
                self.salvastaja = self.turn6()
            xy = self.salvastaja
            XandY = self.MoveToCordinateControl(self.x,self.y,xy[0],xy[1])
            self.x = XandY[0]
            self.y = XandY[1]
            if self.y<=350:
                self.checkpoint = 7
            self.muutuja+=1
        
        
        elif self.checkpoint == 7:
            self.x +=self.speed
