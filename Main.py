import pygame
import Menu, Money, Enemy, Gun, kingdom
from random import randint
import ctypes 


pygame.init()
WIDTH = 800
HEIGHT  = 525
screen = pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption('Save Way')
clock = pygame.time.Clock()
FPS = 40

running=True
kingdome = kingdom.Kingdom(screen,150)

class Map1:#MAP 15x15 Map creating website - http://pyromancers.com/dungeon-painter-online/
    
    def __init__(self,screen):
        self.screen=screen
        self.screen.fill([255,255,255])
        
        
    def drawMap(self):
        map_img = pygame.image.load("images/map15x15new.png").convert_alpha()
        map_img = pygame.transform.scale(map_img,(525,525))
        self.screen.blit(map_img,(0,0))
    

class KasutajaMenu1:#Kasutaja menü klass
    #Klassis kontrollitakse kasutaja vajutamise kooordinaadid
    
    def kasOnNupuPeal1(x,y,nuppuLocation):
        if x >= nuppuLocation[0] and x <= nuppuLocation[0] + nuppuLocation[2]:
            if y >= nuppuLocation[1] and y <= nuppuLocation[1] + nuppuLocation[3]:
                return True
                
    def kasOnNupuPeal2(x,y,nuppuLocation):
        if x >= nuppuLocation[0] and x <= nuppuLocation[0] + nuppuLocation[2]:
            if y >= nuppuLocation[1] and y <= nuppuLocation[1] + nuppuLocation[3]:
                return True
                
    def kasOnNupuPeal3(x,y,nuppuLocation):
        if x >= nuppuLocation[0] and x <= nuppuLocation[0] + nuppuLocation[2]:
            if y >= nuppuLocation[1] and y <= nuppuLocation[1] + nuppuLocation[3]:
                return True
    def kasOnNupuReady(x,y,nuppuLocation):
        if x >= nuppuLocation[0] and x <= nuppuLocation[0] + nuppuLocation[2]:
            if y >= nuppuLocation[1] and y <= nuppuLocation[1] + nuppuLocation[3]:
                return True
    
    def kasMenu2(x,y):
        if x>=680 and x<=760:
            if y>=0 and y<=25:
                return True
            
    def kasOnRemovePeal(x,y,nuppuLocation):
        if x >= nuppuLocation[0] and x <= nuppuLocation[0] + nuppuLocation[2]:
            if y >= nuppuLocation[1] and y <= nuppuLocation[1] + nuppuLocation[3]:
                return True
    
class KasutajaMenu2:
    
    def kasMenu1(x,y):
        if x>=565 and x<=665:
            if y>=0 and y<=25:
                return True
    

kasutajaMenu1 = KasutajaMenu1()

map = Map1(screen)

press=[]

pressedButton1=False
pressedButton2=False
pressedButton3=False

pressedRemoveButton=False

Button1Rectangle=[]
Button2Rectangle=[]
Button3Rectangle=[]
#Maailma maatriks
clear_map = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
             [1,1,1,1,1,1,1,1,1,1,1,1,1,0,0],
             [1,1,1,1,1,1,1,1,1,1,1,1,1,0,0],
             [1,1,1,1,1,1,1,1,1,1,1,1,1,0,0],
             [0,0,0,0,0,0,0,0,0,0,0,1,1,0,0],
             [0,0,1,1,1,1,1,1,1,1,1,1,1,0,0],
             [0,0,1,1,1,1,1,1,1,1,1,1,1,0,0],
             [0,0,1,1,1,1,1,1,1,1,1,1,1,0,0],
             [0,0,1,1,0,0,0,0,0,0,0,0,0,0,0],
             [0,0,1,1,0,0,0,0,0,1,1,1,1,1,1],
             [0,0,1,1,0,0,0,0,0,1,1,1,1,1,1],
             [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0],
             [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0],
             [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]

beginMoney = 80
minMoney=25

rad_gun1 = 100
cost_gun1 = 30
rad_gun2 = 150
cost_gun2=75
rad_gun3 = 200
cost_gun3=150
GlobalStage = 1
Stage = True

Menu1=True
Menu2=False

xMove=0
yMove=randint(35,105)
muutuja=0
YMuutuja=0
RocketX=120
RocketY=155

enemy=[]
fat_man=[]

guns=[]
bullets=[]

#Väike aken, mis tekib mängi võitmisel/kaotamisel
def Mbox(title, text, style):
    return ctypes.windll.user32.MessageBoxW(0, text, title, style)

#Piltide loomine 
BIGimg_right = pygame.image.load("images/BIGtankRight.png").convert_alpha()
BIGimg_right= pygame.transform.scale(BIGimg_right,(35,35))
BIGimg_left = pygame.image.load("images/BIGtankLeft.png").convert_alpha()
BIGimg_left= pygame.transform.scale(BIGimg_left,(35,35))
BIGimg_down = pygame.image.load("images/BIGtankDown.png").convert_alpha()
BIGimg_down= pygame.transform.scale(BIGimg_down,(35,35))
BIGimg_up = pygame.image.load("images/BIGtankUp.png").convert_alpha()
BIGimg_up= pygame.transform.scale(BIGimg_up,(35,35))

img_right = pygame.image.load("images/tankRight.png").convert_alpha()
img_right= pygame.transform.scale(img_right,(20,20))
img_left = pygame.image.load("images/tankLeft.png").convert_alpha()
img_left= pygame.transform.scale(img_left,(20,20))
img_down = pygame.image.load("images/tankDown.png").convert_alpha()
img_down= pygame.transform.scale(img_down,(20,20))
img_up = pygame.image.load("images/tankUp.png").convert_alpha()
img_up= pygame.transform.scale(img_up,(20,20))

img_green = pygame.image.load("images/green.png").convert_alpha()
img_green= pygame.transform.scale(img_green,(35,35))
img_red = pygame.image.load("images/red.png").convert_alpha()
img_red= pygame.transform.scale(img_red,(35,35))
img_blue = pygame.image.load("images/blue.png").convert_alpha()
img_blue = pygame.transform.scale(img_blue,(35,35))


while running:
    Ready = False
    screen.fill([255,255,255])
    #Joonistan kaarti
    map.drawMap()
    
    pos = pygame.mouse.get_pos()
    xCor = pos[0] // 35   #Vasaku ülemise ruudu koordinaadid, kus on kasutaja hiir
    yCor = pos[1] // 35
    
    Menu.BigBox(screen)
    
    if kingdome.getHP()<=0:   
        Mbox("Oops!","Sa kaotasid! Alusta otsast peale", 1)
        enemy=[]
        fat_man=[]
        guns=[]
        bullets=[]
        GlobalStage = 0
        beginMoney = 80
        Button1Rectangle=[]
        Button2Rectangle=[]
        Button3Rectangle=[]
        kingdome = kingdom.Kingdom(screen,150)
        
    if GlobalStage == 11 and Stage == True and len(enemy)==0 and len(fat_man)==0:
        Mbox("YEAH!","Sa võitsid!!!", 1)
        enemy=[]
        fat_man=[]
        guns=[]
        bullets=[]
        GlobalStage = 0
        beginMoney = 80
        Button1Rectangle=[]
        Button2Rectangle=[]
        Button3Rectangle=[]
    
    if Menu1:
        
        Menu.removeButton(screen)
        Money.drawMoney(screen,round(beginMoney))
        Money.drawStage(screen, GlobalStage)
        kingdome.drawHp(screen)
        Menu.Menu1(screen)
        
        for event in pygame.event.get():
            
            if event.type == pygame.QUIT:
                running = False
                
            elif event.type == pygame.MOUSEBUTTONUP:
                press=pygame.mouse.get_pos()
                
                if press[0]>525:
                    pressedButton1=False
                    pressedButton2=False
                    pressedButton3=False
                    pressedRemoveButton=False
                
                if KasutajaMenu1.kasMenu2(press[0],press[1]):
                    Menu1=False
                    Menu2=True
                    
                if KasutajaMenu1.kasOnRemovePeal(press[0],press[1],Menu.removeButtonLocation()):
                    pressedRemoveButton = True
                
                
                if KasutajaMenu1.kasOnNupuPeal1(press[0],press[1],Menu.ButtonGun1Location()): 
                    pressedButton1=True
                    pressedButton2=False
                    pressedButton3=False
                 
                if KasutajaMenu1.kasOnNupuPeal2(press[0],press[1],Menu.ButtonGun2Location()): 
                    pressedButton1=False
                    pressedButton2=True
                    pressedButton3=False
                
                if KasutajaMenu1.kasOnNupuPeal3(press[0],press[1],Menu.ButtonGun3Location()): 
                    pressedButton1=False
                    pressedButton2=False
                    pressedButton3=True
                if KasutajaMenu1.kasOnNupuReady(press[0],press[1],Menu.ButtonReadyLocation()):
                    Ready = True
                
                if pressedButton1 and pos[0] < 525 and pos[1]<525 and clear_map[yCor][xCor] == 0 :
                    if beginMoney-cost_gun1>=0:
                        beginMoney-=cost_gun1
                        Button1Rectangle.append((xCor*35, yCor*35))
                        gun = Gun.Gun(rad_gun1,xCor*35,yCor*35,[0,255,0])
                        guns.append(gun)
                        clear_map[yCor][xCor] = 2
                   
                if pressedButton2 and pos[0] < 525 and pos[1]<525 and clear_map[yCor][xCor] == 0:
                    if beginMoney-cost_gun2>=0:
                        beginMoney-=cost_gun2
                        Button2Rectangle.append((xCor*35, yCor*35))
                        gun = Gun.Gun(rad_gun2,xCor*35,yCor*35,[255,0,0])
                        guns.append(gun)
                        clear_map[yCor][xCor] = 3
                   
                if pressedButton3 and pos[0] < 525 and pos[1]<525 and clear_map[yCor][xCor] == 0:
                    if beginMoney-cost_gun3>=0:
                        beginMoney-=cost_gun3
                        Button3Rectangle.append((xCor*35, yCor*35))
                        gun = Gun.Gun(rad_gun3,xCor*35,yCor*35,[0,0,255])
                        guns.append(gun)
                        clear_map[yCor][xCor] = 4
                        
                ##################################################
                if pressedRemoveButton and (press[0]//35*35,press[1]//35*35) in Button1Rectangle:
                    indeks = Button1Rectangle.index((press[0]//35*35,press[1]//35*35))
                    Button1Rectangle.remove((press[0]//35*35,press[1]//35*35))
                    clear_map[yCor][xCor]=0
                    del guns[indeks]
                    beginMoney+=cost_gun1/2
                    
                elif pressedRemoveButton and (press[0]//35*35,press[1]//35*35) in Button2Rectangle:
                    indeks = Button2Rectangle.index((press[0]//35*35,press[1]//35*35))
                    Button2Rectangle.remove((press[0]//35*35,press[1]//35*35))
                    clear_map[yCor][xCor]=0
                    del guns[indeks]
                    beginMoney+=cost_gun2/2
                    
                elif pressedRemoveButton and (press[0]//35*35,press[1]//35*35) in Button3Rectangle:
                    indeks = Button3Rectangle.index((press[0]//35*35,press[1]//35*35))
                    Button3Rectangle.remove((press[0]//35*35,press[1]//35*35))
                    clear_map[yCor][xCor]=0
                    del guns[indeks]
                    beginMoney+=cost_gun3/2

        Menu.ButtonGun1(screen)
        Menu.ButtonGun2(screen)
        Menu.ButtonGun3(screen)
        Menu.ButtonReady(screen)
        
        if pressedButton1:
            Menu.ButtonGun1Pressed(screen)
        elif pressedButton2:
            Menu.ButtonGun2Pressed(screen)
        elif pressedButton3:
            Menu.ButtonGun3Pressed(screen)
        elif pressedRemoveButton:
            Menu.removeButtonPressed(screen)
        
        
        if pressedButton1 == True or pressedButton2 == True or pressedButton3 == True:
            if pos[0] < 525 and pos[1]<525:
                if pressedButton1 == True and clear_map[yCor][xCor] == 0: #Kui kasutaja valis "Rohelist" nupu siis joonistatakse seda relva koos tema raadiusega"
                    width = 35
                    screen.blit(img_green,(xCor*width,yCor*width))
                    pygame.draw.ellipse(screen,[0,255,0],[xCor*width - rad_gun1 +35/2,yCor*width - rad_gun1 +35/2,rad_gun1*2,rad_gun1*2],2)
                elif pressedButton2 == True and clear_map[yCor][xCor] == 0:
                    width = 35
                    screen.blit(img_red,(xCor*width,yCor*width))
                    pygame.draw.ellipse(screen,[0,255,0],[xCor*width - rad_gun2 + 35/2,yCor*width - rad_gun2 + 35/2,rad_gun2*2,rad_gun2*2],3)
                elif pressedButton3 and clear_map[yCor][xCor] == 0:
                    width = 35
                    screen.blit(img_blue,(xCor*width,yCor*width))
                    pygame.draw.ellipse(screen,[0,255,0],[xCor*width - rad_gun3+35/2,yCor*width - rad_gun3+35/2,rad_gun3*2,rad_gun3*2],3)
    
        #Raha osa
        if beginMoney<cost_gun1:
            pressedButton1=False
        if beginMoney<cost_gun2:
            pressedButton2=False
        if beginMoney<cost_gun3:
            pressedButton3=False
            
    else: ######################MENU2
        Menu.Menu2(screen)
        Menu.reeglid(screen)
        for event in pygame.event.get():
            
            if event.type == pygame.QUIT:
                running = False
                
            elif event.type == pygame.MOUSEBUTTONUP:
                press=pygame.mouse.get_pos()
                
                if KasutajaMenu2.kasMenu1(press[0],press[1]):
                    Menu1=True
                    Menu2=False
    
    #Siin on n.ö. Hard Code, kus käsitsi oli kirjutatud iga taseme raskus
    if Stage == True and Ready == True:
        if GlobalStage == 1:
            for i in range(2):
                a=Enemy.Enemy(screen,-i*25,35,1,(GlobalStage*5000)**(8/12),kingdom,10)
                enemy.append(a)
        elif GlobalStage == 2:
            for i in range(7):
                a=Enemy.Enemy(screen,-i*25,35,1.5,(GlobalStage*5500)**(8/12),kingdom,10)
                enemy.append(a)
        elif GlobalStage == 3:
            for i in range(9):
                a=Enemy.Enemy(screen,-i*25,35,2.7,(GlobalStage*5000)**(8/12),kingdom,15)
                enemy.append(a)
        elif GlobalStage == 4:
            for i in range(7):
                a=Enemy.Enemy(screen,-i*25,35,1.5,(GlobalStage*5000)**(8/12),kingdom,10)
                enemy.append(a)
            enemy.append(Enemy.Enemy(screen,-500,35,2,(GlobalStage*5000)**(8/12),kingdom,10))
            enemy.append(Enemy.Enemy(screen,-600,35,2,(GlobalStage*5000)**(8/12),kingdom,10))
        elif GlobalStage == 4:
            for i in range(7):
                a=Enemy.Enemy(screen,-i*25,35,1.5,(GlobalStage*5000)**(8/12),kingdom,10)
                enemy.append(a)
            enemy.append(Enemy.Enemy(screen,-500,35,2,(GlobalStage*5000)**(8/12),kingdom,10))
            enemy.append(Enemy.Enemy(screen,-600,35,2,(GlobalStage*5000)**(8/12),kingdom,10))
        elif GlobalStage == 5:
            for i in range(13):
                a=Enemy.Enemy(screen,-i*25,35,2,(GlobalStage*6000)**(8/12),kingdom,15)
                enemy.append(a)
        elif GlobalStage == 6:
            for i in range(7):
                a=Enemy.Enemy(screen,-i*25,35,1,(GlobalStage*10000)**(8/12),kingdom,10)
                enemy.append(a)
        elif GlobalStage == 7:
            fat_man.append(Enemy.Fat_Man(screen,0,35,0.7,1500,kingdom,25,1,2))
        elif GlobalStage == 8:
            for i in range(5):
                a=Enemy.Enemy(screen,-i*25,35,1.7,(GlobalStage*5000)**(8/12),kingdom,15)
                enemy.append(a)
            for i in range(5):
                a=Enemy.Enemy(screen,-i*25,35,1.4,(GlobalStage*7000)**(8/12),kingdom,10)
                enemy.append(a)
            for i in range(5):
                a=Enemy.Enemy(screen,-i*25,35,1,(GlobalStage*9000)**(8/12),kingdom,10)
                enemy.append(a)
        elif GlobalStage == 9:
            for i in range(5):
                a=Enemy.Enemy(screen,-i*25,35,2.5,(GlobalStage*5000)**(8/12),kingdom,10)
                enemy.append(a)
            for i in range(3):
                a=Enemy.Enemy(screen,-i*25,35,3,(GlobalStage*10000)**(8/12),kingdom,15)
                enemy.append(a)
            fat_man.append(Enemy.Fat_Man(screen,0,35,0.7,1600,kingdom,25,1,2))
        elif GlobalStage == 10:
            for i in range(2):
                fat_man.append(Enemy.Fat_Man(screen,0,35,0.7,1500,kingdom,25,1,2))
            
        Stage = False
    if len(enemy)==0 and len(fat_man)==0 and Stage==False and GlobalStage != 11:
        GlobalStage+=1
        Stage=True
    
    
                
    if len(Button1Rectangle)!=0: #Kui kasutaja on valinud mingit relva ning asendanud seda väljakule, siis seda joonistakse kaardi peale
        for i in Button1Rectangle:
            screen.blit(img_green,(i[0],i[1]))
    if len(Button2Rectangle)!=0:
        for i in Button2Rectangle:
            screen.blit(img_red,(i[0],i[1]))
    if len(Button3Rectangle)!=0:
        for i in Button3Rectangle:
            screen.blit(img_blue,(i[0],i[1]))
    
    #Siin toimub iga elava vaenlase kontroll. 
    for i in enemy:
        if i.getFinish()==True:
            kingdome.dealdamage(25)
            beginMoney-=i.getMoney()
        if i.getHp()<=0:
            beginMoney+=i.getMoney()
            enemy.remove(i)
        if i.getCheckpoint() == 1 or i.getCheckpoint() == 5 or i.getCheckpoint() == 7:
            i.draw(img_right)
        elif i.getCheckpoint() == 2 or i.getCheckpoint() == 4:
            i.draw(img_down)
        elif i.getCheckpoint() == 3:
            i.draw(img_left)
        else:
            i.draw(img_up)
        i.finishMethod()
        
    for i in fat_man:
        if i.getX()>525:
            fat_man.remove(i)
        if i.getFinish()==True:
            kingdome.dealdamage(30)
        if i.getHp()<=0:
            beginMoney+=i.getMoney()
            #Kui n.ö. peamine peamine vaenlane sureb, saab temast veel 4 vaenlast
            if i.getLevel() > 1:
                fat_man.append(Enemy.Fat_Man(screen,i.getX()-17,i.getY()-17,1,i.getstartHP()/4,kingdom,10,i.getCheckpoint(),i.getLevel()-1))
                fat_man.append(Enemy.Fat_Man(screen,i.getX()-17,i.getY()+17,1,i.getstartHP()/4,kingdom,10,i.getCheckpoint(),i.getLevel()-1))
                fat_man.append(Enemy.Fat_Man(screen,i.getX()+17,i.getY()-17,1,i.getstartHP()/4,kingdom,10,i.getCheckpoint(),i.getLevel()-1))
                fat_man.append(Enemy.Fat_Man(screen,i.getX()+17,i.getY()+17,1,i.getstartHP()/4,kingdom,10,i.getCheckpoint(),i.getLevel()-1))
            elif i.getLevel() == 1:
                enemy.append(Enemy.Enemy(screen,i.getX()-17,i.getY()-17,1,(GlobalStage*5000)**(8/12),kingdom,10))
                enemy.append(Enemy.Enemy(screen,i.getX()-17,i.getY()-17,1,(GlobalStage*5000)**(8/12),kingdom,10))
                enemy.append(Enemy.Enemy(screen,i.getX()-17,i.getY()-17,1,(GlobalStage*5000)**(8/12),kingdom,10))
                enemy.append(Enemy.Enemy(screen,i.getX()-17,i.getY()-17,1,(GlobalStage*5000)**(8/12),kingdom,10))
            fat_man.remove(i)
        
        #Piltide joonistamine vastavalt sellest, kuhu on pööratud vaenlane
        if i.getCheckpoint() == 1 or i.getCheckpoint() == 5 or i.getCheckpoint() == 7:
            i.draw(BIGimg_right)
        elif i.getCheckpoint() == 2 or i.getCheckpoint() == 4:
            i.draw(BIGimg_down)
        elif i.getCheckpoint() == 3:
            i.draw(BIGimg_left)
        else:
            i.draw(BIGimg_up)
        
        i.finishMethod()
    
    for i in guns:
        if i.checkInRaius(fat_man)==True:
            if i.getColor()==[0,255,0]:
                bullets.append(Gun.Bullet(i.getX()+31/2,i.getY()+31/2,i.getEnemy(),15,screen,i,i.getColor(),25))
            elif i.getColor()==[255,0,0]:
                bullets.append(Gun.Bullet(i.getX()+31/2,i.getY()+31/2,i.getEnemy(),12.5,screen,i,i.getColor(),50))
            else:
                bullets.append(Gun.Bullet(i.getX()+31/2,i.getY()+31/2,i.getEnemy(),10,screen,i,i.getColor(),110))
            i.setReady(False)
        if i.checkInRaius(enemy)==True:
            if i.getColor()==[0,255,0]:
                bullets.append(Gun.Bullet(i.getX()+31/2,i.getY()+31/2,i.getEnemy(),15,screen,i,i.getColor(),25))
            elif i.getColor()==[255,0,0]:
                bullets.append(Gun.Bullet(i.getX()+31/2,i.getY()+31/2,i.getEnemy(),12.5,screen,i,i.getColor(),55))
            else:
                bullets.append(Gun.Bullet(i.getX()+31/2,i.getY()+31/2,i.getEnemy(),10,screen,i,i.getColor(),110))
            i.setReady(False)
    for i in bullets:
        i.finishMetod()
        if i.getAlive() == False:
            bullets.remove(i)
            
            
    pygame.display.flip()
    clock.tick(FPS)

pygame.quit()
                

                


