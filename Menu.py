import pygame
pygame.init()

teksti_font_Buttons = pygame.font.Font(None, 20)
teksti_font_Menu = pygame.font.Font(None, 20)
teksti_font_Remove = pygame.font.Font(None, 40)
teksti_font_Reeglid = pygame.font.SysFont("comicsansms", 9)

def Menu1(screen):
    x=550
    pygame.draw.lines(screen, [0,0,0], False, [(x,25), (x+15,0), (x+100,0), (x+115,25)], 3)
def Menu2(screen):
    x=550
    pygame.draw.lines(screen, [0,0,0], False, [(115+x,25), (130+x,0), (130+x+80,0), (130+x+95,25)], 3)

def BigBox(screen):
    x=550
    pygame.draw.rect(screen,[0,0,0],[550,25,225,500],1)
    pygame.draw.lines(screen, [0,0,0], False, [(x,25), (x+15,0), (x+100,0), (x+115,25)], 1)
    pygame.draw.lines(screen, [0,0,0], False, [(115+x,25), (130+x,0), (130+x+80,0), (130+x+95,25)], 1)
    
    menu1=teksti_font_Menu.render("Relvad",1,[0,0,0])
    screen.blit(menu1,[585,10])
    menu2=teksti_font_Menu.render("Reeglid",1,[0,0,0])
    screen.blit(menu2,[700,10])
    
def ButtonReady(screen):
    pygame.draw.rect(screen,[0,0,0],[573,475,60,20],1)
    description=teksti_font_Buttons.render("Valmis",1,[0,0,0])
    screen.blit(description,[580,480])
    
def ButtonGun1(screen):
     pygame.draw.rect(screen,[0,0,0],[573,75,180,100],1)
     
     description=teksti_font_Buttons.render("Roheline",1,[0,0,0])
     cost = teksti_font_Buttons.render("Price: 30$",1,[0,0,0])
     damage = teksti_font_Buttons.render("Damage: low",1,[0,0,0])
     
     screen.blit(description,[635,80])
     screen.blit(cost,[630,140])
     screen.blit(damage,[620,110])
     
def ButtonGun1Pressed(screen):
     pygame.draw.rect(screen,[0,255,0],[573,75,180,100],1)
     
def ButtonGun2(screen):
     pygame.draw.rect(screen,[0,0,0],[573,220,180,100],1)
     description=teksti_font_Buttons.render("Punane",1,[0,0,0])
     cost = teksti_font_Buttons.render("Price: 75$",1,[0,0,0])
     damage = teksti_font_Buttons.render("Damage: middle",1,[0,0,0])
     
     screen.blit(description,[635,225])
     screen.blit(cost,[630,285])
     screen.blit(damage,[620,255])
     
def ButtonGun2Pressed(screen):
     pygame.draw.rect(screen,[0,255,0],[573,220,180,100],1)
     
def ButtonGun3(screen):
     pygame.draw.rect(screen,[0,0,0],[573,360,180,100],1)
     
     description=teksti_font_Buttons.render("Sinine",1,[0,0,0])
     cost = teksti_font_Buttons.render("Price: 150$",1,[0,0,0])
     damage = teksti_font_Buttons.render("Damage: high",1,[0,0,0])
     
     screen.blit(description,[635,370])
     screen.blit(cost,[630,430])
     screen.blit(damage,[620,400])
    
def ButtonGun3Pressed(screen):
     pygame.draw.rect(screen,[0,255,0],[573,360,180,100],1)
     
def ButtonReadyLocation():
    return (573,475,60,20)
def ButtonGun1Location():
    return (573,75,180,100)
def ButtonGun2Location():
    return (573,220,180,100)
def ButtonGun3Location():
    return (573,360,180,100)
def removeButtonLocation():
    return (725,475,35,35)

def removeButton(screen):
    pygame.draw.rect(screen,[0,0,0],[725,475,35,35],1)
    description=teksti_font_Remove.render("$",1,[0,0,0])
    screen.blit(description,[735,480])
def removeButtonPressed(screen):
    pygame.draw.rect(screen,[0,255,0],[725,475,35,35],1)
    
def reeglid(screen):
    desc=teksti_font_Reeglid.render("Relvade ostmine, müümine ja paigutamine",1,[0,0,0])
    screen.blit(desc,[555,30])
    description=teksti_font_Reeglid.render("1. Relva paigutamiseks klõpsa relva peale ja aseta",1,[0,0,0])
    screen.blit(description,[555,50])
    description1=teksti_font_Reeglid.render("seda rohelisele alale. Teise kohta on keelatud.",1,[0,0,0])
    screen.blit(description1,[555,60])
    description2=teksti_font_Reeglid.render("2. Igal relval on oma hind, raadius ja oma",1,[0,0,0])
    screen.blit(description2,[555,80])
    description3=teksti_font_Reeglid.render("kahjustus. Olenevalt hinnast on relval suurem",1,[0,0,0])
    screen.blit(description3,[555,90])
    description4=teksti_font_Reeglid.render("raadius ja suurem kahju vaenlasele",1,[0,0,0])
    screen.blit(description4,[555,100])
    description5=teksti_font_Reeglid.render("3. Relva müümiseks vajuta nuppu $ ja klõpsa relvale,",1,[0,0,0])
    screen.blit(description5,[555,120])
    description6=teksti_font_Reeglid.render("mida tahad müüa. Müümise eest saad tagasi ainult",1,[0,0,0])
    screen.blit(description6,[555,130])
    description7=teksti_font_Reeglid.render("poole esialgsest hinnast (arvesta sellega).",1,[0,0,0])
    screen.blit(description7,[555,140])
    desc=teksti_font_Reeglid.render("Mängureeglid ja eesmärk",1,[0,0,0])
    screen.blit(desc,[555,180])
    description8=teksti_font_Reeglid.render("Pärast relvade paigutamist võid alustada mängu.",1,[0,0,0])
    screen.blit(description8,[555,200])
    description9=teksti_font_Reeglid.render("Selleks vajuta nupp Valmis. Sama nupu kõrval võid",1,[0,0,0])
    screen.blit(description9,[555,210])
    description1=teksti_font_Reeglid.render("märgata, et sul on olemas ka HP (elud) ja tase.",1,[0,0,0])
    screen.blit(description1,[555,220])
    description2=teksti_font_Reeglid.render("Mängus on kokku 10 taset ning iga tasemega mäng",1,[0,0,0])
    screen.blit(description2,[555,230])
    description3=teksti_font_Reeglid.render("läheb raskemaks. Mängu alguses on sulle antud",1,[0,0,0])
    screen.blit(description3,[555,240])
    description4=teksti_font_Reeglid.render("150 HP (elud). Kui need saavad otsa, siis see",1,[0,0,0])
    screen.blit(description4,[555,250])
    description5=teksti_font_Reeglid.render("tähendab, et sa kaotasid ning pead alustama algu-",1,[0,0,0])
    screen.blit(description5,[555,260])
    description6=teksti_font_Reeglid.render("sest peale. ",1,[0,0,0])
    screen.blit(description6,[555,270])
    description7=teksti_font_Reeglid.render("Raha saad siis, kui vaenlane sureb. Olenevalt ",1,[0,0,0])
    screen.blit(description7,[555,280])
    description8=teksti_font_Reeglid.render("vaenlase tugevusest saad kas rohkem või vähem",1,[0,0,0])
    screen.blit(description8,[555,290])
    description9=teksti_font_Reeglid.render("raha.",1,[0,0,0])
    screen.blit(description9,[555,300])
    description7=teksti_font_Reeglid.render("Sinu eesmärgiks on mitte lasta vaenlastel jõuda",1,[0,0,0])
    screen.blit(description7,[555,310])
    description8=teksti_font_Reeglid.render("mängukaardi lõpuotsa ehk proovida salvestada oma",1,[0,0,0])
    screen.blit(description8,[555,320])
    description9=teksti_font_Reeglid.render("mänguelud mängu lõpuni.",1,[0,0,0])
    screen.blit(description9,[555,330])
    
    description9=teksti_font_Reeglid.render("EDU MÄNGIMISES!!!",1,[0,0,0])
    screen.blit(description9,[610,390])
    
    
    
    
    
    
